package com.boarsoft.rpc.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	private static final Logger log = LoggerFactory.getLogger(Main.class);
	private static ClassPathXmlApplicationContext ctx;

	public static void main(String[] args) {
		ctx = new ClassPathXmlApplicationContext("classpath:conf/context.xml");
		System.out.println("Startup ".concat(ctx.isRunning() ? "successfully." : "failed."));

		try {
			// RpcReferenceConfig rc = new RpcReferenceConfig("demo", "demo1",
			// //
			// "com.boarsoft.rpc.demo.DemoService", "1.0.0", "demoService");
			// rc.setTimeout(6000);
			// rc.setMocker("demoMocker");
			// RpcCore.getCurrentInstance().registReferece(rc);
//			DemoService ds = ctx.getBean("demoService", DemoService.class);
//			Object ro = ds.helloSC(new User("Mac_J"));
//			log.info("Result = {}", ro);

			// TomcatServer ts = ctx.getBean(TomcatServer.class);
			// ts.getTomcat().getServer().await();
		} finally {
			// ctx.close();
		}
		// System.exit(0);
	}

}
