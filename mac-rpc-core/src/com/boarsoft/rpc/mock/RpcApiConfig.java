package com.boarsoft.rpc.mock;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.boarsoft.common.Util;
import com.boarsoft.rpc.bean.RpcMethodConfig;

/**
 * 支持泛化调用等场景的外挂式RPC配置
 */
public class RpcApiConfig implements ApplicationContextAware {
	private static ApplicationContext applicationContext;

	private static Map<String, RpcMethodConfig> configMap;

	static {
		init();
	}

	public static void init() {
		// TODO 从api.json读取外置的api配置
		// FileUtil.read(new File("文件路径"), "UTF-8");
		// JSONArray ja = JSON.parseArray
		// 将JSONArray转为configMap
		// configMap.put
	}

	@Override
	public void setApplicationContext(ApplicationContext ac) throws BeansException {
		applicationContext = ac;
	}

	/**
	 * TODO 兼容带参和不带参的方法签名
	 * 
	 * @param key
	 * @return
	 */
	public static RpcMethodConfig find(String key) {
		// return configMap.get(key);
		return null;
	}

	public static int getTimeout(String key) {
		if (Util.strIsEmpty(key)) {
			return -1;
		}
		RpcMethodConfig v = find(key);
		if (v == null) {
			return -1;
		}
		return v.getTimeout();
	}

	public static boolean hasMocker(String key) {
		if (Util.strIsEmpty(key)) {
			return false;
		}
		RpcMethodConfig v = find(key);
		if (v == null) {
			return false;
		}
		String m = v.getMocker();
		if (Util.strIsEmpty(m)) {
			return false;
		}
		return applicationContext.containsBean(m);
	}

	public static Object getMocker(String key) {
		if (Util.strIsEmpty(key)) {
			return null;
		}
		RpcMethodConfig v = find(key);
		if (v == null) {
			return null;
		}
		String m = v.getMocker();
		if (Util.strIsEmpty(m)) {
			return null;
		}
		if (!applicationContext.containsBean(m)) {
			return null;
		}
		return applicationContext.getBean(m);
	}

	public static String getMocking(String key) {
		if (Util.strIsEmpty(key)) {
			return null;
		}
		RpcMethodConfig v = find(key);
		if (v == null) {
			return null;
		}
		return v.getMocker();
	}

	public static boolean isMocking(String key) {
		if (Util.strIsEmpty(key)) {
			return false;
		}
		RpcMethodConfig v = find(key);
		if (v == null) {
			return false;
		}
		if (Util.strIsEmpty(v.getMocker())) {
			return false;
		}
		return v.isAutoMock();
	}

	public static Map<String, RpcMethodConfig> getConfigMap() {
		return configMap;
	}

	public static void setConfigMap(Map<String, RpcMethodConfig> m) {
		configMap = m;
	}
}