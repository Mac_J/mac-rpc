package com.boarsoft.rpc.generalize;

import com.boarsoft.rpc.bean.RpcMethodConfig;

public interface RpcGenMocker {

	/**
	 * 
	 * @param rmc
	 * @param args
	 * @return
	 */
	Object invoke(RpcMethodConfig rmc, Object[] args);

}
