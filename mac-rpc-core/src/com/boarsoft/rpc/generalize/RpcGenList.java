package com.boarsoft.rpc.generalize;

import java.util.ArrayList;
import java.util.List;

public class RpcGenList extends RpcGenCollection {
	private static final long serialVersionUID = -4631944972199302775L;

	protected RpcGenList() {
		this.collection = new ArrayList<>();
	}

	public RpcGenList(String clazzName) {
		this.clazzName = clazzName;
		this.collection = new ArrayList<>();
	}

	public RpcGenList(String clazzName, List<Object> list) {
		this.clazzName = clazzName;
		this.collection = list;
	}

	public List<Object> getList() {
		return (List<Object>) collection;
	}

	public void setList(List<Object> list) {
		this.setCollection(list);
	}
}