package com.boarsoft.rpc.generalize;

public class RpcGenArray extends RpcGenType {
	private static final long serialVersionUID = -5483942918036394119L;

	/** 为兼容GenericCollection增加：元素类型 */
	protected String elClzName = "java.lang.Object";

	protected Object[] array;

	protected RpcGenArray() {
	}

	public RpcGenArray(Object[] array) {
		this.array = array;
	}
	
	public int getLength() {
		return array.length;
	}
	
	public Object get(int i) {
		return array[i];
	}

	@Override
	public Object[] specialize() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		if (array == null) {
			return null;
		}
		Object[] arr = new Object[array.length];
		for (int i = 0; i < array.length; i++) {
			Object a = array[i];
			if (a != null && a instanceof RpcGenType) {
				Object o = ((RpcGenType) a).specialize();
				arr[i] = o;
			} else {
				arr[i] = a;
			}
		}
		return arr;
	}

	public String getElClzName() {
		return elClzName;
	}

	public void setElClzName(String elClzName) {
		this.elClzName = elClzName;
	}

	public Object[] getArray() {
		return array;
	}

	public void setArray(Object[] array) {
		this.array = array;
	}

}
