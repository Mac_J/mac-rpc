package com.boarsoft.rpc.generalize;

import java.util.HashSet;
import java.util.Set;

public class RpcGenSet extends RpcGenCollection {
	private static final long serialVersionUID = -4631944972199302775L;

	protected RpcGenSet() {
		this.collection = new HashSet<>();
	}

	public RpcGenSet(String clazzName) {
		this.clazzName = clazzName;
		this.collection = new HashSet<>();
	}

	public RpcGenSet(String clazzName, Set<Object> set) {
		this.clazzName = clazzName;
		this.collection = set;
	}

	public Set<Object> getSet() {
		return (Set<Object>) collection;
	}

	public void setSet(Set<Object> set) {
		this.setCollection(set);
	}
}