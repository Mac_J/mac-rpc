package com.boarsoft.rpc.generalize;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.boarsoft.common.util.JsonUtil;

public class RpcGenMap extends RpcGenType {
	private static final long serialVersionUID = -4631944972199302775L;

	protected Map<Object, Object> map = new HashMap<>();

	/** 为兼容GenericCollection增加：元素类型 */
	protected String elClzName = "java.lang.Object";

	public RpcGenMap() {
	}

	public RpcGenMap(String clazzName) {
		this.clazzName = clazzName;
	}

	public RpcGenMap(String clazzName, Map<Object, Object> map) {
		this.clazzName = clazzName;
		this.map = map;
	}

	@Override
	public String toString() {
		return JsonUtil.toJSONString(this);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Object, Object> specialize()
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Map<Object, Object> tm = (Map<Object, Object>) this.create(clazzName);
		for (Entry<Object, Object> en : map.entrySet()) {
			Object k = en.getKey();
			Object v = en.getValue();
			if (v != null && v instanceof RpcGenType) {
				Object o = ((RpcGenType) v).specialize();
				tm.put(k, o);
			} else {
				tm.put(k, v);
			}
		}
		return tm;
	}

	// @Override
	public int size() {
		return map.size();
	}

	// @Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	// @Override
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	// @Override
	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	// @Override
	public Object get(Object key) {
		return map.get(key);
	}

	// @Override
	public Object put(Object key, Object value) {
		return map.put(key, value);
	}

	// @Override
	public Object remove(Object key) {
		return map.remove(key);
	}

	// @Override
	public void putAll(Map<? extends Object, ? extends Object> m) {
		map.putAll(m);
	}

	// @Override
	public void clear() {
		map.clear();
	}

	// @Override
	public Set<Object> keySet() {
		return map.keySet();
	}

	// @Override
	public Collection<Object> values() {
		return map.values();
	}

	// @Override
	public Set<java.util.Map.Entry<Object, Object>> entrySet() {
		return map.entrySet();
	}

	public Map<Object, Object> getMap() {
		return map;
	}

	public void setMap(Map<Object, Object> map) {
		this.map = map;
	}

	public String getElClzName() {
		return elClzName;
	}

	public void setElClzName(String elClzName) {
		this.elClzName = elClzName;
	}
}