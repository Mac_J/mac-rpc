package com.boarsoft.rpc.adapter;

import java.lang.reflect.Method;
import java.util.List;

import com.boarsoft.rpc.bean.RpcMethodConfig;
import com.boarsoft.rpc.bean.RpcRegistry;
import com.boarsoft.rpc.core.RpcContext;

/**
 * 供第三方扩展的服务提供者查找器
 * 
 * @author Mac_J
 *
 */
public interface RpcProviderFinder {
	/**
	 * 返回符合条件的服务提供者
	 * 
	 * @param rpcContext
	 * @param sign
	 * @param gen
	 * @return
	 */
	List<RpcRegistry> find(RpcContext rpcContext, String sign, boolean gen, RpcMethodConfig rmc, Object[] args, Method method);

	/**
	 * 服务引用信息全部注册完成事件
	 */
	void afterRegistReferences(RpcContext rpcContext);
}