package com.boarsoft.rpc.adapter;

import java.util.Map;

import com.boarsoft.rpc.bean.RpcReferenceConfig;
import com.boarsoft.rpc.bean.RpcServiceConfig;

/**
 * 与第三方RPC（协议）的扩展（事件处理）
 *
 */
public interface RpcProtocolExtension {
	/**
	 * 执行协议适配器初始化前的扩展动作
	 * 
	 * @param adapter
	 */
	void beforeInit(RpcProtocolAdapter adapter);

	/**
	 * 在协议适配器注册完服务后的扩展动作
	 * 
	 * @param adapter
	 * @param params
	 */
	void afterRegistService(RpcProtocolAdapter adapter, RpcServiceConfig sc, Map<String, Object> params);

	/**
	 * 在协议适配器注册服务引用前的扩展动作
	 * 
	 * @param adapter
	 * @param rc
	 */
	void beforeRegistReference(RpcProtocolAdapter adapter, RpcReferenceConfig rc);
}
