package com.boarsoft.rpc.adapter;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

import com.boarsoft.rpc.bean.RpcMethodConfig;
import com.boarsoft.rpc.bean.RpcReferenceConfig;
import com.boarsoft.rpc.bean.RpcServiceConfig;

/**
 * 与第三方RPC（协议）的集成
 *
 */
public interface RpcProtocolAdapter {

	/**
	 * 注册服务提供者，生成服务代理类 或 单纯只注册服务提供者
	 * 
	 * @param sc
	 * @param export
	 *            为true表示要export服务，为false表示单纯只注册服务提供者
	 * @return 返回true表示注册成功，false表示适配器不支持注册，需要执行进行默认注册动作
	 * @throws ClassNotFoundException
	 */
	boolean registService(RpcServiceConfig sc, boolean export) throws ClassNotFoundException;

	/**
	 * 注册服务消费者，生成引用代理类 或 单纯只注册服务消费者
	 * 
	 * @param rc
	 * @return 返回代理对象
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 */
	Future<?> registReference(RpcReferenceConfig rc) throws ClassNotFoundException, InterruptedException;

	/**
	 * 泛化调用
	 * 
	 * @param rc
	 * @param rmc
	 * @param args
	 * @return
	 * @throws Exception
	 */
	Object invoke(RpcReferenceConfig rc, RpcMethodConfig rmc, Object[] args) throws Exception;

	/**
	 * 
	 * @param rc
	 * @param await
	 *            是否等待注册中心返回服务提供者信息
	 * @return
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 */
	boolean subscribe(RpcReferenceConfig rc, boolean await) throws ClassNotFoundException, InterruptedException;

	/**
	 * 
	 * @param rc
	 * @return
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 * @throws TimeoutException
	 */
	Object referReference(RpcReferenceConfig rc)
			throws ClassNotFoundException, InterruptedException, ExecutionException, TimeoutException;

	/**
	 * 
	 * @param <T>
	 * @param rc
	 * @param clazz
	 * @return
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 * @throws TimeoutException
	 */
	<T> T referReference(RpcReferenceConfig rc, Class<T> clazz)
			throws ClassNotFoundException, InterruptedException, ExecutionException, TimeoutException;

	/**
	 * 返回当前适配器实现的协议名
	 * 
	 * @return
	 */
	String getProtocol();
}
