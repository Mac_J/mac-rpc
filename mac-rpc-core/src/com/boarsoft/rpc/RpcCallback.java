package com.boarsoft.rpc;

public interface RpcCallback {
	/**
	 * 
	 * @param result 产生的结果，如果有的话
	 * @param e 产生的异常，如果有的话
	 * @param args 原方法参数
	 */
	void callback(Object result, Throwable e, Object... args);
}
