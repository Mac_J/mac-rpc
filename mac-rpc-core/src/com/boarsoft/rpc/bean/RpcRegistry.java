package com.boarsoft.rpc.bean;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.boarsoft.common.Util;
import com.boarsoft.rpc.RpcConfig;

public class RpcRegistry implements Serializable {
	private static final long serialVersionUID = 1262809103083863664L;

	protected Date version = new Date();
	/** node addr */
	protected String key;
	/** master */
	protected String master;
	/** 放附加属性，如：http.port */
	protected Properties meta = new Properties();
	/** k: refereceId */
	protected Map<String, RpcReferenceConfig> referenceMap = new HashMap<String, RpcReferenceConfig>();
	/** k: service sign */
	protected Map<String, RpcServiceConfig> serviceMap = new HashMap<String, RpcServiceConfig>();
	/** 上一次心跳时间 */
	protected long lastBeat = System.currentTimeMillis();

	public RpcRegistry() {
		this(RpcConfig.getAddr());
	}

	public RpcRegistry(String addr) {
		this.key = addr;
		this.meta.putAll(System.getProperties());
		this.meta.putAll(RpcConfig.getProperties());
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o instanceof RpcRegistry) {
			return this.key.equals(((RpcRegistry) o).key);
		}
		if (o instanceof String) {
			return this.key.equals((String) o);
		}
		return false;
	}

	@Override
	public String toString() {
		return key;
	}

	@Override
	public RpcRegistry clone() throws CloneNotSupportedException {
		return (RpcRegistry) super.clone();
	}

	public RpcServiceConfig getServiceConfig(String sign) {
		return serviceMap.get(sign);
	}

	public String getMetaString(String key, String value) {
		return meta.getProperty(key, value);
	}

	public int getMetaInt(String key, int value) {
		return Util.str2int(meta.getProperty(key), value);
	}

	public int getServiceMethodId(String serviceKey, Method method) {
		RpcServiceConfig sc = this.serviceMap.get(serviceKey);
		RpcMethodConfig mc = sc.getMethodConfig(method);
		return mc.getRelativeId();
	}

	public Map<String, RpcReferenceConfig> getReferenceMap() {
		return this.referenceMap;
	}

	public void setReferenceMap(Map<String, RpcReferenceConfig> referenceMap) {
		this.referenceMap = referenceMap;
	}

	public Map<String, RpcServiceConfig> getServiceMap() {
		return this.serviceMap;
	}

	public void setServiceMap(Map<String, RpcServiceConfig> serviceMap) {
		this.serviceMap = serviceMap;
	}

	public Date getVersion() {
		return this.version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Properties getMeta() {
		return meta;
	}

	public void setMeta(Properties meta) {
		this.meta = meta;
	}

	public String getMaster() {
		return master;
	}

	public void setMaster(String master) {
		this.master = master;
	}

	public long getLastBeat() {
		return lastBeat;
	}

	public void setLastBeat(long lastBeat) {
		this.lastBeat = lastBeat;
	}
}
