package com.boarsoft.rpc.http.jetty.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.boarsoft.rpc.bean.RpcCall;
import com.boarsoft.rpc.bean.RpcInvoking;
import com.boarsoft.rpc.bean.RpcMethodConfig;

public interface IoHandler {
	/**
	 * 
	 * @param mc
	 *            RpcMethodConfig 可能为空，由具体的实现类处理
	 * @param addr
	 *            远程节点的地址（IP:PORT）
	 * @param req
	 * @param rsp
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	RpcInvoking read(RpcMethodConfig mc, String addr, HttpServletRequest req, HttpServletResponse rsp)
			throws IOException, ClassNotFoundException;

	/**
	 * 
	 * @param co
	 * @param rsp
	 * @throws IOException
	 */
	void write(RpcCall co, HttpServletResponse rsp) throws IOException;

}