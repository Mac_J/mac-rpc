package com.boarsoft.rpc.spring.cloud;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.cloud.client.serviceregistry.Registration;

import com.boarsoft.rpc.RpcConfig;
import com.boarsoft.rpc.bean.RpcServiceConfig;

public class MacRpcRegistration implements Registration {
	protected RpcServiceConfig serviceConfig;

	public MacRpcRegistration(RpcServiceConfig serviceConfig) {
		this.serviceConfig = serviceConfig;
	}

	@Override
	public String getServiceId() {
		return serviceConfig.getRef();
	}

	@Override
	public String getHost() {
		return RpcConfig.getIp();
	}

	@Override
	public int getPort() {
		return RpcConfig.getInt("rpc.http.port", 8080);
	}

	@Override
	public boolean isSecure() {
		return false;
	}

	@Override
	public URI getUri() {
		StringBuilder sb = new StringBuilder().append("/")//
				.append(serviceConfig.getRef());
		try {
			return new URI(sb.toString());
		} catch (URISyntaxException e) {
			return null;
		}
	}

	@Override
	public Map<String, String> getMetadata() {
		Map<String, String> meta = new HashMap<String, String>();
		meta.put("group", serviceConfig.getGroup());
		meta.put("name", serviceConfig.getName());
		meta.put("interface", serviceConfig.getInterfaceName());
		meta.put("version", serviceConfig.getVersion());
		return meta;
	}

}
