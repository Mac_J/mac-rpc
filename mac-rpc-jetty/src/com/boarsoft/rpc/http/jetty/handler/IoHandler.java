package com.boarsoft.rpc.http.jetty.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.boarsoft.rpc.bean.RpcCall;
import com.boarsoft.rpc.bean.RpcInvoking;
import com.boarsoft.rpc.bean.RpcMethodConfig;

/**
 * 用于从HTTP请求中获取RPC调用信息<br>
 * 通过HTTP响应对象返回RPC调用结果
 * 
 * @author Mac_J
 *
 */
public interface IoHandler {
	/**
	 * 从HTTP请深圳市中读取RPC调用信息
	 * 
	 * @param mc
	 *            RpcMethodConfig 可能为空，由具体的实现类处理
	 * @param addr
	 *            远程节点的地址（IP:PORT）
	 * @param req
	 *            HTTP请求
	 * @param rsp
	 *            HTTP响应
	 * @return RPC调用信息
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	RpcInvoking read(RpcMethodConfig mc, String addr, HttpServletRequest req, HttpServletResponse rsp)
			throws IOException, ClassNotFoundException;

	/**
	 * 通过HTTP响应对象返回RPC调用结果
	 * 
	 * @param co
	 *            RPC调用结果
	 * @param rsp
	 *            HTTP响应
	 * @throws IOException
	 */
	void write(RpcCall co, HttpServletResponse rsp) throws IOException;
}