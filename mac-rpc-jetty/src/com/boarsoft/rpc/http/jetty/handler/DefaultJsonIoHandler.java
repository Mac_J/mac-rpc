package com.boarsoft.rpc.http.jetty.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.boarsoft.common.Util;
import com.boarsoft.common.util.HttpClientUtil;
import com.boarsoft.rpc.bean.RpcCall;
import com.boarsoft.rpc.bean.RpcInvoking;
import com.boarsoft.rpc.bean.RpcMethodConfig;
import com.boarsoft.rpc.http.mapper.DefaultParamMapper;
import com.boarsoft.rpc.http.mapper.ParamMapper;

/**
 * 解析客户端/前端以XML POST形式提交的字符串，此字符串必须依序包含所有的参数值，形如：<br>
 * xxx.xxx.Xxx={...}
 * 
 * @author Mac_J
 *
 */
public class DefaultJsonIoHandler extends AbstractJsonIoHandler {

	protected ParamMapper paramMapper = new DefaultParamMapper();

	@Override
	public RpcInvoking read(RpcMethodConfig mc, String addr, HttpServletRequest req, HttpServletResponse rsp)
			throws IOException, ClassNotFoundException {
		RpcInvoking ri = super.read(mc, addr, req, rsp);
		RpcCall co = ri.getRpcCall();
		String body = HttpClientUtil.readStr(req.getInputStream(), charset);
		if (Util.strIsEmpty(body)) {
			co.setArguments(RpcCall.EMPTY_ARGS);
			return ri;
		}
		//
		String[] arr = body.split("\n");
		Object[] args = new Object[arr.length];
		for (int i = 0; i < arr.length; i++) {
			String ln = arr[i];
			int j = ln.indexOf("=");
			String className = ln.substring(0, j);
			String json = ln.substring(j + 1);
			if ("null".equals(json)) {
				args[i] = null;
				continue;
			}
			Class<?> clazz = Class.forName(className);
			paramMapper.map(args, i, clazz, null, json, req, rsp);
		}
		// TODO 检查参数
		co.setArguments(args);
		return ri;
	}

	public ParamMapper getParamMapper() {
		return paramMapper;
	}

	public void setParamMapper(ParamMapper paramMapper) {
		this.paramMapper = paramMapper;
	}

}
