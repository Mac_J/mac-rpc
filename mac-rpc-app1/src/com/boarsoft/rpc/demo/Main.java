package com.boarsoft.rpc.demo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	private static ClassPathXmlApplicationContext ctx;

	public static void main(String[] args) {
		ctx = new ClassPathXmlApplicationContext("classpath:conf/context.xml");
		System.out.println("Startup ".concat(ctx.isRunning() ? "successfully." : "failed."));

		User o = new User("Mac_J");
		// RpcCall c = new RpcCall();
		// c.setArguments(new Object[] { o, 10, 1000 });
		// byte[] b = RpcSerializer.serialize(c);
		// System.out.println(b.length);

		// RpcSvcSpy svcSpy = ctx.getBean("rpcSvcSpy", RpcSvcSpy.class);
		// String mk =
		// "demo/demo1/com.boarsoft.rpc.sample.DemoService/1.0.0/helloSC(com.boarsoft.rpc.sample.User)";
		// svcSpy.down(mk, true);
		// svcSpy.setResult(mk, "service mock result");

		DemoService ds = ctx.getBean("demoServiceRemote", DemoService.class);
		System.out.println(ds.hello(o));
	}
}
