package com.boarsoft.rpc.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component("demoService2")
public class DemoServiceImpl2 implements DemoService2 {
	private static final Logger log = LoggerFactory.getLogger(DemoServiceImpl2.class);

	@Override
	public User2 genTest1(User2 u) {
		log.info("genTest1 {}", u);
		if (u == null || StringUtils.isEmpty(u.getName())) {
			throw new IllegalArgumentException("Invalid parameter");
		}
		return new User2("Hello ".concat(u.getName()));
	}
}