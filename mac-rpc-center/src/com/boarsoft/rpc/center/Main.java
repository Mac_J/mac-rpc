package com.boarsoft.rpc.center;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	private static ClassPathXmlApplicationContext ctx;

	public static void main(String[] args) {
		ctx = new ClassPathXmlApplicationContext("classpath:conf/context.xml");
		System.out.println("Startup ".concat(ctx.isRunning() ? "successfully." : "failed."));
	}
}
