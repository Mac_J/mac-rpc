package com.boarsoft.rpc.web;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.transaction.jta.JtaAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;

import com.boarsoft.rpc.core.RpcCore;

@ImportResource(locations = { "classpath:spring/context.xml" })
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
		JtaAutoConfiguration.class, AopAutoConfiguration.class, MongoAutoConfiguration.class })
@EnableAspectJAutoProxy(proxyTargetClass = true)
@DependsOn("rpcCore")
public class Application {
	public Application(RpcCore rpcCore) {
		// Nothing to do
	}

	public static void main(String[] args) throws NumberFormatException, IOException {
		ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
		System.out.println("Startup ".concat(ctx.isRunning() ? "successfully." : "failed."));
	}
}