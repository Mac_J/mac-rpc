package com.boarsoft.rpc.demo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	private static ClassPathXmlApplicationContext ctx;

	public static void main(String[] args) throws NoSuchMethodException, SecurityException {
		System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		ctx = new ClassPathXmlApplicationContext("classpath:conf/context.xml");
		System.out.println("Startup ".concat(ctx.isRunning() ? "successfully." : "failed."));
	}
}
