package com.boarsoft.rpc.jetty.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.boarsoft.common.util.FileUtil;
import com.boarsoft.common.util.HttpClientUtil;
import com.boarsoft.common.util.JsonUtil;
import com.boarsoft.common.util.StreamUtil;
import com.boarsoft.rpc.demo.User;

public class JettyTest {

	@Test
	public void test1() throws IOException {
		Map<String, String> headers = new HashMap<String, String>();
		// headers.put("service",
		// "demo/demo1/com.boarsoft.rpc.demo.DemoService/1.0");
		// headers.put("method", "hello(com.boarsoft.rpc.demo.User)");

		User u = new User("Mac_J");
		String body = new StringBuilder()//
				.append("com.boarsoft.rpc.demo.User=")//
				.append(JsonUtil.toJSONString(u))//
				.toString();

		String url = "http://localhost:8103/demo/hello2.do";
		String rs = HttpClientUtil.sendPost(url, "application/json", headers, body);
		System.out.println(rs);
	}

	@Test
	public void test2() throws IOException {
		Map<String, String> headers = new HashMap<String, String>();
		// headers.put("service",
		// "demo/demo1/com.boarsoft.rpc.demo.DemoService/1.0");
		// headers.put("method", "hello(com.boarsoft.rpc.demo.User)");

		User u = new User("Mac_J");
		Map<String, String> params = new HashMap<String, String>();
		params.put("user", JSON.toJSONString(u));

		String url = "http://localhost:8103/demo/hello2.do";
		String rs = HttpClientUtil.sendPost(url, "application/x-www-form-urlencoded", headers, params);
		System.out.println(rs);
	}

	@Test
	public void test3() throws IOException {
		Map<String, String> headers = new HashMap<String, String>();
		// headers.put("service",
		// "demo/demo1/com.boarsoft.rpc.demo.DemoService/1.0");
		// headers.put("method", "hello(com.boarsoft.rpc.demo.User)");

		User u = new User("Mac_J");
		String body = new StringBuilder()//
				.append("com.boarsoft.rpc.demo.User=")//
				.append(JsonUtil.toJSONString(u))//
				.toString();

		String url = "https://localhost:8443/demo/hello2.do";
		System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		String rs = sendPost(url, "application/json", "UTF-8", "UTF-8", headers, body, null);
		System.out.println(rs);
	}

	public static String sendPost(String url, String contentType, //
			String encOut, String encIn, Map<String, String> headers, String body, String cookie)
			throws UnsupportedEncodingException, IOException {
		PrintWriter out = null;
		BufferedReader br = null;
		HttpURLConnection conn = null;
		StringBuilder sb = new StringBuilder();
		try {
			conn = (HttpsURLConnection) new URL(url).openConnection();
			conn.setConnectTimeout(3000);
			conn.setReadTimeout(30000);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0");
			conn.setRequestProperty("Content-Type", contentType);
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 设置cookie
			if (cookie != null) {
				conn.setRequestProperty("Cookie", cookie);
			}
			// headers
			if (headers != null && headers.size() > 0) {
				for (String h : headers.keySet())
					conn.setRequestProperty(h, headers.get(h));
			}
			// body
			out = new PrintWriter(conn.getOutputStream());
			out.print(body);
			out.flush();
			br = new BufferedReader(new InputStreamReader(conn.getInputStream(), encIn));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line).append(FileUtil.LINE_SEPARATOR);
			}
		} finally {
			StreamUtil.close(br);
			StreamUtil.close(out);
			if (conn != null) {
				conn.disconnect();
			}
		}
		return sb.toString();
	}
}
