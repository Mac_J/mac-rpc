package com.boarsoft.boar.mac.rpc.service.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.common.Constants;
import com.boarsoft.common.Util;
import com.boarsoft.common.dao.PagedResult;
import com.boarsoft.rpc.bean.RpcMethodConfig;
import com.boarsoft.rpc.bean.RpcReferenceConfig;
import com.boarsoft.rpc.bean.RpcRegistry;
import com.boarsoft.rpc.bean.RpcServiceConfig;
import com.boarsoft.rpc.core.RpcContext;
import com.boarsoft.rpc.core.RpcCore;
import com.boarsoft.rpc.core.RpcKeeper;
import com.boarsoft.rpc.core.RpcLink;
import com.boarsoft.soagov.config.BwConfig;
import com.boarsoft.soagov.config.BwConfigImpl;
import com.boarsoft.soagov.config.SlaConfig;
import com.boarsoft.soagov.config.SlaConfigImpl;
import com.boarsoft.soagov.spy.SvcSpy;

@RestController
@RequestMapping("/svc")
public class RpcServiceAction {
	private static final Logger log = LoggerFactory.getLogger(RpcServiceAction.class);
//	/** 服务开关与升降级 */
//	private static final int ACT_UP = 1;
//	/** 服务开关与升降级 */
//	private static final int ACT_DOWN = 2;
//	/** 服务开关与升降级 */
//	private static final int ACT_MOCK = 3;
//	/** 增加或修改服务限流配置 */
//	private static final int ACT_LIMIT_SAVE = 4;
//	/** 移除服务限流配置 */
//	private static final int ACT_LIMIT_REMOVE = 5;
//	/** 启用服务限流、开启监控，是否真正限流取决于流量上限 */
//	private static final int ACT_LIMIT_ON = 6;
//	/** 停用服务限流、关闭监控 */
//	private static final int ACT_LIMIT_OFF = 7;
//	/** 增加或修改服务限流配置 */
//	private static final int ACT_BW_SAVE = 8;
//	/** 移除服务限流配置 */
//	private static final int ACT_BW_REMOVE = 9;
//	/** 启用黑白名单 */
//	private static final int ACT_BW_ON = 10;
//	/** 停用黑白名单 */
//	private static final int ACT_BW_OFF = 11;

	@Autowired
	protected RpcContext rpcContext;
	@Autowired
	private RpcKeeper rpcKeeper;
	@Autowired
	private RpcCore rpcCore;

	@Autowired
	@Lazy(value = true)
	private SvcSpy svcSpy;

	/**
	 * 返回指定服务接口的方法信息，以及服务的提供者和消费者列表
	 * 
	 * @param key
	 *            服务接口签名
	 * @return
	 */
	@RequestMapping("/get.do")
	// @Authorized(code = "rpc.svc.get")
	public ReplyInfo<Object> get(String key) {
		List<Map<String, Object>> pLt = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> cLt = new ArrayList<Map<String, Object>>();
		Map<String, RpcRegistry> rrMap = rpcContext.getRegistryMap();
		for (RpcRegistry rr : rrMap.values()) {
			Map<String, RpcServiceConfig> scMap = rr.getServiceMap();
			if (scMap.containsKey(key)) {
				RpcServiceConfig sc = scMap.get(key);
				Map<String, Object> m = new HashMap<String, Object>();
				m.put("addr", rr.getKey());
				m.put("mocker", sc.getMocker());
				m.put("ref", sc.getRef());
				m.put("timeout", sc.getTimeout());
				m.put("type", sc.getType());
				m.put("id", sc.getId());
				m.put("methods", sc.getMethodConfigMap().size());
				m.put("status", 0);
				pLt.add(m);
			}
			Map<String, RpcReferenceConfig> rcMap = rr.getReferenceMap();
			if (rcMap.containsKey(key)) {
				RpcReferenceConfig rc = rcMap.get(key);
				Map<String, Object> m = new HashMap<String, Object>();
				m.put("addr", rr.getKey());
				m.put("mocker", rc.getMocker());
				m.put("id", rc.getId());
				m.put("timeout", rc.getTimeout());
				m.put("type", rc.getType());
				m.put("methods", rc.getMethodConfigMap().size());
				cLt.add(m);
			}
		}
		Map<String, Object> rm = new HashMap<String, Object>();
		rm.put("providers", pLt);
		rm.put("consumers", cLt);
		return new ReplyInfo<Object>(true, rm);
	}

	/**
	 * 分页返回所有的服务接口
	 * 
	 * @param pageNo
	 *            页码
	 * @param pageSize
	 *            页大小
	 * @return
	 */
	@RequestMapping("/list.do")
	// @Authorized(code = "rpc.svc.list")
	public ReplyInfo<Object> list(int pageNo, int pageSize) {
		Map<String, Integer> tpMap = new HashMap<String, Integer>();
		Map<String, Integer> trMap = new HashMap<String, Integer>();
		Map<String, RpcServiceConfig> tsMap = new HashMap<String, RpcServiceConfig>();
		//
		Map<String, RpcRegistry> rrMap = rpcContext.getRegistryMap();
		for (RpcRegistry rr : rrMap.values()) {
			Map<String, RpcServiceConfig> scMap = rr.getServiceMap();
			tsMap.putAll(scMap);
			for (String sk : scMap.keySet()) {
				Integer i = tpMap.get(sk);
				tpMap.put(sk, i == null ? 0 : i + 1);
			}
			Map<String, RpcReferenceConfig> rcMap = rr.getReferenceMap();
			for (String sk : rcMap.keySet()) {
				Integer i = trMap.get(sk);
				trMap.put(sk, i == null ? 0 : i + 1);
			}
		}
		List<Map<String, Object>> rmLt = new ArrayList<Map<String, Object>>();
		for (String sk : tsMap.keySet()) {
			RpcServiceConfig sc = tsMap.get(sk);
			//
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("group", sc.getGroup());
			m.put("name", sc.getName());
			m.put("interface", sc.getInterfaceName());
			m.put("version", sc.getVersion());
			m.put("sign", sc.getSign());
			m.put("providers", tpMap.get(sk));
			m.put("consumers", trMap.get(sk));
			//
			List<Map<String, Object>> dLt = new ArrayList<Map<String, Object>>();
			Map<String, RpcMethodConfig> mcMap = sc.getMethodConfigMap();
			for (String mk : mcMap.keySet()) {
				RpcMethodConfig mc = mcMap.get(mk);
				Map<String, Object> d = new HashMap<String, Object>();
				// d.put("callback", mc.getCallback());
				// d.put("failover", mc.getFailover());
				// d.put("mocker", mc.getMocker());
				// d.put("protocol", mc.getProtocol());
				// d.put("relativeId", mc.getRelativeId());
				d.put("sign", mc.getSign());
				// d.put("timeout", mc.getTimeout());
				// d.put("type", mc.getType());
				// d.put("uri", mc.getUri());
				dLt.add(d);
			}
			m.put("methods", dLt);
			//
			rmLt.add(m);
		}
		PagedResult<Map<String, Object>> pr = //
				new PagedResult<Map<String, Object>>(rmLt.size(), rmLt, pageNo, pageSize);
		return new ReplyInfo<Object>(true, pr);
	}

	/**
	 * 对服务进行治理操作
	 * 
	 * @param code
	 *            服务接口签名
	 * @param key
	 *            匹配条件（限流、黑白名单等）
	 * @param addr
	 *            （被通知）目标节点地址
	 * @param act
	 *            服务治理操作，详见操作清单
	 * @param mockType
	 *            服务模拟类型
	 * @param mockJson
	 *            服务模拟数据
	 * @param tpsLimit
	 *            TPS上限
	 * @param status
	 *            服务状态
	 * @return
	 */
	@RequestMapping("/up.do")
	// @Authorized(code = "svc.inst.up")
	public ReplyInfo<Object> up(String addr, String code) {
		boolean success = false;
		RpcContext.specify2(addr);
		try {
			success = svcSpy.up(code);
		} catch (Exception e) {
			log.error("Error on toggle remote service {} at {}", code, addr, e);
		} finally {
			RpcContext.specify2(null);
		}
		if (success) {
			return ReplyInfo.SUCCESS;
		}
		return new ReplyInfo<Object>(Constants.ERROR_UNKNOWN);
	}

	@RequestMapping("/mock.do")
	// @Authorized(code = "svc.inst.mock")
	public ReplyInfo<Object> mock(String addr, String code, String key, String mockType, String mockJson) {
		boolean success = false;
		RpcContext.specify2(addr);
		try {
			if (Util.strIsEmpty(mockType) || Util.strIsEmpty(mockJson)) {
				success = svcSpy.mock(code);
			} else {
				try {
					success = svcSpy.mock(code, mockType, mockJson);
				} catch (ClassNotFoundException e) {
					log.error("Mock type or json is invalid", e);
				}
			}
		} catch (Exception e) {
			log.error("Error on toggle remote service {} at {}", code, addr, e);
		} finally {
			RpcContext.specify2(null);
		}
		if (success) {
			return ReplyInfo.SUCCESS;
		}
		return new ReplyInfo<Object>(Constants.ERROR_UNKNOWN);
	}

	@RequestMapping("/down.do")
	// @Authorized(code = "svc.inst.down")
	public ReplyInfo<Object> down(String addr, String code) {
		boolean success = false;
		RpcContext.specify2(addr);
		try {
			success = svcSpy.down(code);
		} catch (Exception e) {
			log.error("Error on toggle remote service {} at {}", code, addr, e);
		} finally {
			RpcContext.specify2(null);
		}
		if (success) {
			return ReplyInfo.SUCCESS;
		}
		return new ReplyInfo<Object>(Constants.ERROR_UNKNOWN);
	}

	@RequestMapping("/limit/save.do")
	// @Authorized(code = "svc.inst.limit.save")
	public ReplyInfo<Object> limitSave(String addr, String code, String key, int tpsLimit) {
		boolean success = false;
		RpcContext.specify2(addr);
		try {
			if (Util.strIsEmpty(key)) {
				success = svcSpy.limit(code, tpsLimit);
			} else {
				SlaConfig sc = new SlaConfigImpl(key);
				success = svcSpy.limit(code, sc, tpsLimit);
			}
		} catch (Exception e) {
			log.error("Error on toggle remote service {} at {}", code, addr, e);
		} finally {
			RpcContext.specify2(null);
		}
		if (success) {
			return ReplyInfo.SUCCESS;
		}
		return new ReplyInfo<Object>(Constants.ERROR_UNKNOWN);
	}

	@RequestMapping("/limit/remove.do")
	// @Authorized(code = "svc.inst.unlimit")
	public ReplyInfo<Object> limitRemove(String addr, String code, String key) {
		boolean success = false;
		RpcContext.specify2(addr);
		try {
			if (Util.strIsEmpty(key)) {
				success = svcSpy.unlimit(code);
			} else {
				success = svcSpy.unlimit(code, key);
			}
		} catch (Exception e) {
			log.error("Error on toggle remote service {} at {}", code, addr, e);
		} finally {
			RpcContext.specify2(null);
		}
		if (success) {
			return ReplyInfo.SUCCESS;
		}
		return new ReplyInfo<Object>(Constants.ERROR_UNKNOWN);
	}

	@RequestMapping("/limit/on.do")
	// @Authorized(code = "svc.inst.limit.on")
	public ReplyInfo<Object> limitOn(String addr, String code, String key) {
		boolean success = false;
		RpcContext.specify2(addr);
		try {
			success = svcSpy.setSlaConfigOn(code, true);
		} catch (

		Exception e) {
			log.error("Error on toggle remote service {} at {}", code, addr, e);
		} finally {
			RpcContext.specify2(null);
		}
		if (success) {
			return ReplyInfo.SUCCESS;
		}
		return new ReplyInfo<Object>(Constants.ERROR_UNKNOWN);
	}

@RequestMapping("/limit/off.do")
// @Authorized(code = "svc.inst.limit.off")
public ReplyInfo<Object> limitOff(String addr, String code, String key) {
	boolean success = false;
	RpcContext.specify2(addr);
	try {
			success = svcSpy.setSlaConfigOn(code, false);
	}catch(
	Exception e)
	{
		log.error("Error on toggle remote service {} at {}", code, addr, e);
	}finally
	{
		RpcContext.specify2(null);
	}if(success)
	{
		return ReplyInfo.SUCCESS;
	}return new ReplyInfo<Object>(Constants.ERROR_UNKNOWN);
}

@RequestMapping("/bw/save.do")
// @Authorized(code = "svc.inst.bw.save")
public ReplyInfo<Object> bwSave(String addr, String code, String key, int status) {
	boolean success = false;
	RpcContext.specify2(addr);
	try {
			BwConfig sc = new BwConfigImpl(key, status == 0);
			success = svcSpy.addBwConfig(code, sc);
	}catch(

	Exception e)
	{
		log.error("Error on toggle remote service {} at {}", code, addr, e);
	}finally
	{
		RpcContext.specify2(null);
	}if(success)
	{
		return ReplyInfo.SUCCESS;
	}return new ReplyInfo<Object>(Constants.ERROR_UNKNOWN);
}

@RequestMapping("/bw/remove.do")
// @Authorized(code = "svc.inst.limit")
public ReplyInfo<Object> bwRemove(String addr, String code, String key, int status) {
	boolean success = false;
	RpcContext.specify2(addr);
	try {
			success = svcSpy.addBwConfig(code, new BwConfigImpl(key));
	}catch(

	Exception e)
	{
		log.error("Error on toggle remote service {} at {}", code, addr, e);
	}finally
	{
		RpcContext.specify2(null);
	}if(success)
	{
		return ReplyInfo.SUCCESS;
	}return new ReplyInfo<Object>(Constants.ERROR_UNKNOWN);
}
@RequestMapping("/bw/on.do")
// @Authorized(code = "svc.inst.bw.on")
public ReplyInfo<Object> limit(String addr, String code, String key) {
	boolean success = false;
	RpcContext.specify2(addr);
	try {
			success = svcSpy.setBwConfigOn(code, true);
	}catch(Exception e)
	{
		log.error("Error on toggle remote service {} at {}", code, addr, e);
	}finally
	{
		RpcContext.specify2(null);
	}if(success)
	{
		return ReplyInfo.SUCCESS;
	}return new ReplyInfo<Object>(Constants.ERROR_UNKNOWN);
}

	@RequestMapping("/bw/off.do")
	// @Authorized(code = "svc.inst.bw.off")
	public ReplyInfo<Object> toggle(String code, String key, String addr, int act, String mockType, String mockJson,
			int tpsLimit, int status) {
		boolean success = false;
		RpcContext.specify2(addr);
		try {
				success = svcSpy.setBwConfigOn(code, false);
		} catch (Exception e) {
			log.error("Error on toggle remote service {} at {}", code, addr, e);
		} finally {
			RpcContext.specify2(null);
		}
		if (success) {
			return ReplyInfo.SUCCESS;
		}
		return new ReplyInfo<Object>(Constants.ERROR_UNKNOWN);
	}

	/**
	 * 启用某个服务提供者
	 * 
	 * @param addr
	 *            服务提供者节点地址
	 * @param key
	 *            服务接口签名
	 * @param target
	 *            （被通知）目标节点地址
	 * @return
	 */
	@RequestMapping("/enable.do")
	// @Authorized(code = "mac.rpc.svc.enable")
	public ReplyInfo<Object> enable(String addr, String key, String target) {
		try {
			this.toggle(RpcMethodConfig.ID_ENABLE_PROVIDER, addr, key, target);
			return ReplyInfo.SUCCESS;
		} catch (Throwable e) {
			log.error("Error on invoke ENABLE_PROVIDER {}/{} on {} ", addr, key, target, e);
			return ReplyInfo.FAILED;
		}
	}

	/**
	 * 禁用某个服务提供者
	 * 
	 * @param addr
	 *            服务提供者节点地址
	 * @param key
	 *            服务接口签名
	 * @param target
	 *            （被通知）目标节点地址
	 * @return
	 */
	@RequestMapping("/disable.do")
	// @Authorized(code = "mac.rpc.svc.disable")
	public ReplyInfo<Object> disable(String addr, String key, String target) {
		try {
			this.toggle(RpcMethodConfig.ID_DISABLE_PROVIDER, addr, key, target);
			return ReplyInfo.SUCCESS;
		} catch (Throwable e) {
			log.error("Error on invoke DISABLE_PROVIDER {}/{} on {} ", addr, key, target, e);
			return ReplyInfo.FAILED;
		}
	}

	protected ReplyInfo<Object> toggle(int methodId, String addr, String key, String target) throws Throwable {
		// target为空表示要将某个服务上线或下线，广播通知所有节点，节点addr将开始提供或不再提供key服务
		if (Util.strIsEmpty(target)) {
			// addr为空表示所有节点都开始提供或不再提供key服务
			// key为表示指定节点开始提供或不再提供任何服务
			// addr和key两者都为空表示所有节点都开始提供或不再提供任何服务
			RpcMethodConfig mc = RpcContext.getMyMethodConfig(methodId);
			Map<String, Object> rm = rpcCore.broadcast(mc, new Object[] { addr, key }, null);
			return new ReplyInfo<Object>(true, rm);
		}
		String[] addrs = addr.split(",");
		for (String aimAddr : addrs) {
			// target不为空表示要通知某个节点，节点addr将不再（向其）提供key服务
			RpcLink lo = rpcCore.link2(target);
			// addr为空表示所有节点都开始提供或不再（向其）提供key服务
			// key为表示指定节点开始提供或不再（向其）提供任何服务
			// addr和key两者都为空表示所有节点都开始提供或不再（向其）提供任何服务
			lo.invoke(methodId, new Object[] { aimAddr, key });
		}
		return ReplyInfo.SUCCESS;
	}

	public RpcContext getRpcContext() {
		return rpcContext;
	}

	public void setRpcContext(RpcContext rpcContext) {
		this.rpcContext = rpcContext;
	}

	public RpcKeeper getRpcKeeper() {
		return rpcKeeper;
	}

	public void setRpcKeeper(RpcKeeper rpcKeeper) {
		this.rpcKeeper = rpcKeeper;
	}

	public RpcCore getRpcCore() {
		return rpcCore;
	}

	public void setRpcCore(RpcCore rpcCore) {
		this.rpcCore = rpcCore;
	}

	public SvcSpy getSvcSpy() {
		return svcSpy;
	}

	public void setSvcSpy(SvcSpy svcSpy) {
		this.svcSpy = svcSpy;
	}
}
