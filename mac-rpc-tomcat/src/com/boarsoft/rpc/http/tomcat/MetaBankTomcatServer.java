package com.boarsoft.rpc.http.tomcat;

import com.boarsoft.common.Util;
import com.boarsoft.rpc.bean.RpcMethodConfig;
import com.boarsoft.rpc.bean.RpcServiceConfig;
import com.boarsoft.rpc.core.RpcContext;
import com.boarsoft.rpc.http.tomcat.handler.RpcTomcatServlet;
import org.apache.catalina.Host;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 基于Tomcat的服务器包装类<br>
 * 将当前节点的每个RPC服务方法暴露为对应的HTTP服务
 * 
 * @author Mac_J
 *
 */
public class MetaBankTomcatServer {
	private static final Logger log = LoggerFactory.getLogger(MetaBankTomcatServer.class);

	@Autowired
	protected RpcContext rpcContext;

	/** */
	Map<String,RpcTomcatServlet> rpcTomcatServletMap =  new HashMap<String,RpcTomcatServlet>();
	/** */
	protected Tomcat tomcat = new Tomcat();

	/** */
	protected String hostname = "localhost";
	/** */
	protected int port = 8080;
	/** */
	protected String contextPath = "";
	/** */
	protected String suffix = ".do";

	@PostConstruct
	public void init() throws Exception {
		String tomcatPort  = System.getProperties().getProperty("tomcat.port");
		port = StringUtils.isEmpty(tomcatPort)?port:Integer.valueOf(tomcatPort);
		tomcat.setHostname(hostname);
		// 创建上下文
		StandardContext context = new StandardContext();
		context.setPath(contextPath);
		context.addLifecycleListener(new Tomcat.FixContextListener());

		Host host = tomcat.getHost();
		host.setAutoDeploy(false);
		host.addChild(context);
		for(Map.Entry<String,RpcTomcatServlet> rpcTomcatServletEntry : rpcTomcatServletMap.entrySet()){
			tomcat.addServlet(contextPath,rpcTomcatServletEntry.getKey(),rpcTomcatServletEntry.getValue());
			context.addServletMappingDecoded("/" + rpcTomcatServletEntry.getKey(),rpcTomcatServletEntry.getKey());
		}
		Connector connector = new Connector();
		connector.setPort(port);
		tomcat.getService().addConnector(connector);
		tomcat.start();
		log.info("Tomcat embed start at {} successfully.", port);
	}

	@PreDestroy
	public void stop() {
		if (this.tomcat == null) {
			return;
		}
		try {
			this.tomcat.stop();
			this.tomcat = null;
		} catch (Exception e) {
			log.error("Error on stop jetty server", e);
		}
	}

	public RpcContext getRpcContext() {
		return rpcContext;
	}

	public void setRpcContext(RpcContext rpcContext) {
		this.rpcContext = rpcContext;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public Tomcat getTomcat() {
		return tomcat;
	}

	public void setTomcat(Tomcat tomcat) {
		this.tomcat = tomcat;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public Map<String, RpcTomcatServlet> getRpcTomcatServletMap() {
		return rpcTomcatServletMap;
	}

	public void setRpcTomcatServletMap(Map<String, RpcTomcatServlet> rpcTomcatServletMap) {
		this.rpcTomcatServletMap = rpcTomcatServletMap;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
}
